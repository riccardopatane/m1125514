## Quali differenze si ritiene distinguano i due algoritmi di mining?

Nello streamMine l’iterazione è interna allo stream e quindi nascosta grazie alla presenza delle lambda.
Questo aumenta la leggibilità del codice rispetto al metodo threadMine.
threadMine è il metodo più rapido.
Ho notato una leggero rallentamento in streamMine se uso uno stream di interi rispetto a uno stream generico ma penso sia solamente una mia impressione.