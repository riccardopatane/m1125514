package pcd2017.exer2;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;

/**
 * Suggested element for the Stream-based algorithm.
 */
class Candidate {
  final int testNonce;
  final String hash;

  Candidate(int testNonce, String hash) {
    this.testNonce = testNonce;
    this.hash = hash;
  }
}

public class Miner {

  /**
   * Parallel-stream-based mining algorithm.
   * 
   * @param data
   * @return
   */
  public Block streamMine(BlockData data) {
    Integer n = IntStream.iterate(Integer.MIN_VALUE, i->++i).parallel()   //creo uno stream di Integer parallelizato che parte dal valore minimo rappresentabile
              .filter(i->isTarget(Miner.sha256(Miner.canonical(data, i))))  //filtro solo e conservo solo gli interi che rispettano la consegna
              .findFirst().getAsInt();                                      //restituisco il PRIMO risultato utile e lo converto ad intero
      return new Block(n, Miner.sha256(Miner.canonical(data, n)), data);    //creo un nuovo blocco utilizzando il nonce trovato
  }

  /**
   * Thread-based mining algorithm.
   * 
   * @param data
   * @return
   * @throws InterruptedException
   * @throws ExecutionException
   */
  public Block threadMine(BlockData data) {
    int nt = 10;
      ExecutorService es = Executors.newWorkStealingPool(nt);
      AtomicInteger c = new AtomicInteger(Integer.MIN_VALUE);//avere un contatore che può essere modificto in modo atomico mi evita sovvrapposizioni
      ArrayList<Callable<Integer>> colist = new ArrayList<>();
      for(int i=0; i<nt; ++i) colist.add(() -> {            //aggiungo alla inizialmente vuota collezione di liste una nuova collable
          boolean check = false;
          int n=3;
          while(!check) {
              n = c.getAndIncrement();
              check = Miner.validate(new Block(n, Miner.sha256(Miner.canonical(data, n)), data));//verifico se ho trovato il nonce corretto
          }
          return n;
          });
      int nonce = es.invokeAny(colist); // ritorno il primo nonce trovato, rimane in attesa fino a che non accade tipo il Future
      es.shutdown();                    //interrompo tutti i thread che non hanno ultimato la ricerca perchè non più necessaria
      return new Block(nonce, Miner.sha256(Miner.canonical(data, nonce)), data);
  }

  /**
   * Create the canonical representation of the blockdata with the nonce, to be fed to the hash function.
   * 
   * DO NOT MODIFY
   * 
   * @param input data to hash
   * @param nonce nonce to hash with the data
   * @return
   */
  public static String canonical(BlockData input, int nonce) {
    return nonce + ":" + input.previousHash + ":" + input.timestamp + ":"
        + input.data;
  }

  /**
   * Calculate the hash of the input
   * 
   * DO NOT MODIFY
   * 
   * @param input data to hash
   * @return hash value
   */
  public static String sha256(String input) {
    try {
      MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
      byte[] result = sha256.digest(input.getBytes());
      return bytesToHex(result);
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException("This should not have happened");
    }
  }

  /**
   * Pretty-prints a byte array.
   * 
   * DO NOT MODIFY
   * 
   * @param hash
   * @return
   */
  private static String bytesToHex(byte[] hash) {
    StringBuilder hexString = new StringBuilder();
    for (int i = 0; i < hash.length; i++) {
      String hex = Integer.toHexString(0xff & hash[i]);
      if (hex.length() == 1)
        hexString.append('0');
      hexString.append(hex);
    }
    return hexString.toString();
  }

  public static final String HASH_PREFIX = "42424";

  /**
   * Checks if an hash is the target value
   * 
   * DO NOT MODIFY
   * 
   * @param hash the hash to check
   * @return true if it satisfies the target condition
   */
  public static boolean isTarget(Candidate candidate) {
    return candidate.hash.startsWith(HASH_PREFIX);
  }

  /**
   * Checks if an hash is the target value
   * 
   * DO NOT MODIFY
   * 
   * @param hash the hash to check
   * @return true if it satisfies the target condition
   */
  public static boolean isTarget(String hash) {
    return hash.startsWith(HASH_PREFIX);
  }

  /**
   * Checks if the block is valid
   * 
   * DO NOT MODIFY
   * 
   * @param block Block to validate
   * @return true if the block hash is correct and in target
   */
  public static boolean validate(Block block) {
    boolean targetHash = isTarget(block.hash);
    boolean correctHash = block.hash
        .equals(sha256(canonical(block.data, block.nonce)));
    return targetHash && correctHash;
  }

}
